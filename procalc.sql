-- Adminer 4.8.1 MySQL 5.5.5-10.8.4-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `calcs`;
CREATE TABLE `calcs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `calcs` (`id`, `user_id`, `title`, `uuid`, `created_at`, `updated_at`) VALUES
(1,	1,	'123123',	'7155806e-38d8',	NULL,	NULL);

DROP TABLE IF EXISTS `elements`;
CREATE TABLE `elements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `calc_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_id` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'col-12',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Заголовок',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Описание',
  `title_visibility` tinyint(1) NOT NULL DEFAULT 1,
  `description_visibility` tinyint(1) NOT NULL DEFAULT 1,
  `padding_top` int(11) NOT NULL DEFAULT 16,
  `padding_bottom` int(11) NOT NULL DEFAULT 16,
  `word` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `elements` (`id`, `calc_id`, `uid`, `style_id`, `order`, `size`, `title`, `description`, `title_visibility`, `description_visibility`, `padding_top`, `padding_bottom`, `word`) VALUES
(1,	NULL,	NULL,	1,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	NULL),
(2,	NULL,	NULL,	2,	0,	'col-12',	'Заголовок',	'Описание',	1,	0,	16,	16,	NULL),
(3,	NULL,	NULL,	3,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	NULL),
(4,	NULL,	NULL,	4,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	NULL),
(5,	NULL,	NULL,	5,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	NULL),
(6,	NULL,	NULL,	6,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	NULL),
(7,	NULL,	NULL,	7,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	NULL),
(8,	NULL,	NULL,	8,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	NULL),
(9,	'7155806e-38d8',	'_b17ybybcw',	1,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	'A'),
(10,	'7155806e-38d8',	'_g463yfzf9',	8,	0,	'col-12',	'Заголовок',	'Описание',	1,	1,	16,	16,	'B');

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2019_08_19_000000_create_failed_jobs_table',	1),
(4,	'2019_12_14_000001_create_personal_access_tokens_table',	1),
(5,	'2022_04_25_145219_create_calcs_table',	1),
(6,	'2022_04_25_152818_create_elements_table',	1),
(7,	'2022_04_25_152830_create_options_table',	1),
(8,	'2022_06_02_110908_elements_options',	1),
(9,	'2022_07_20_151542_create_options_images_table',	1),
(10,	'2022_07_27_115456_create_options_buttons_table',	1),
(11,	'2022_07_27_210845_create_options_results_table',	1),
(12,	'2022_09_01_103222_add_option_value',	1),
(13,	'2022_09_01_134457_create_styles_table',	1),
(14,	'2022_09_01_135041_edit_element_prop',	1),
(15,	'2022_09_06_063045_element_word',	1),
(16,	'2022_09_06_080311_option_disable_value',	1);

DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `index` int(11) NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL DEFAULT 0,
  `disable_value` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `options` (`id`, `element_id`, `index`, `text`, `value`, `disable_value`) VALUES
(1,	1,	0,	'Первый',	0,	0),
(2,	1,	1,	'Второй',	0,	0),
(3,	9,	0,	'Первый',	0,	0),
(4,	9,	1,	'Второй',	0,	0),
(5,	9,	2,	'2',	0,	0);

DROP TABLE IF EXISTS `options_buttons`;
CREATE TABLE `options_buttons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'calc',
  `button_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Кнопка',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_align` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'center',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `options_buttons` (`id`, `element_id`, `action`, `button_name`, `phone`, `email`, `url`, `text_align`) VALUES
(1,	8,	'calc',	'Кнопка',	NULL,	NULL,	NULL,	'center'),
(2,	10,	'calc',	'Кнопка',	NULL,	NULL,	NULL,	'center');

DROP TABLE IF EXISTS `options_checkboxes`;
CREATE TABLE `options_checkboxes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `selected_arr` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`selected_arr`)),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `options_checkboxes` (`id`, `element_id`, `selected_arr`) VALUES
(1,	3,	'[1]');

DROP TABLE IF EXISTS `options_images`;
CREATE TABLE `options_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `options_images` (`id`, `element_id`, `path`, `name`) VALUES
(1,	7,	NULL,	NULL);

DROP TABLE IF EXISTS `options_inputs`;
CREATE TABLE `options_inputs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Подсказка',
  `placeholder_visibility` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `options_inputs` (`id`, `element_id`, `input_type`, `placeholder_text`, `placeholder_visibility`) VALUES
(1,	5,	'number',	'Подсказка',	1);

DROP TABLE IF EXISTS `options_results`;
CREATE TABLE `options_results` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Результат',
  `post` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `options_selects`;
CREATE TABLE `options_selects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `selected` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `options_selects` (`id`, `element_id`, `selected`) VALUES
(1,	1,	1),
(2,	4,	1),
(3,	9,	1);

DROP TABLE IF EXISTS `options_sliders`;
CREATE TABLE `options_sliders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `min` int(11) NOT NULL DEFAULT 1,
  `step` int(11) NOT NULL DEFAULT 1,
  `max` int(11) NOT NULL DEFAULT 100,
  `value` int(11) NOT NULL DEFAULT 10,
  `marks` int(11) NOT NULL DEFAULT 10,
  `tooltip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'focus',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `options_sliders` (`id`, `element_id`, `min`, `step`, `max`, `value`, `marks`, `tooltip`) VALUES
(1,	2,	1,	1,	100,	10,	10,	'focus');

DROP TABLE IF EXISTS `options_texts`;
CREATE TABLE `options_texts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `align` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bold` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `italic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` int(11) NOT NULL DEFAULT 15,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `options_texts` (`id`, `element_id`, `align`, `bold`, `italic`, `size`, `text`) VALUES
(1,	6,	'left',	'normal',	'normal',	15,	'Текст рыба для примера чуть менее длимнный, чем прошлый, но тоже в целом подорйдет. Я не очень сильно запарился его писать, но всеже минутку потратил. Ничего страшного');

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `styles`;
CREATE TABLE `styles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `prop` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `styles` (`id`, `prop`, `name`, `color`, `icon`) VALUES
(1,	'select',	'Список',	'#f2c94c',	'icon-list'),
(2,	'slider',	'Слайдер',	'#6fcf97',	'icon-slider'),
(3,	'checkbox',	'Чекбокс',	'#2d9cdb',	'icon-checkbox'),
(4,	'radio',	'Радио кнопка',	'#bb6bd9',	'icon-radio'),
(5,	'input',	'Поле',	'#73ccff',	'icon-input'),
(6,	'text',	'Текст',	'#6fcf97',	'icon-text'),
(7,	'image',	'Картинка',	'#f2994a',	'icon-image'),
(8,	'button',	'Кнопка',	'#C2F0FF',	'icon-button');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'Жлупек Жопный',	'mellanxollik@yandex.ru',	NULL,	'$2y$10$9m0eKNE8om5vE6hVCpIAxefy6mvcKeCoHUai83.I3VhUT6wkhafk2',	NULL,	'2023-03-06 09:02:26',	'2023-03-06 09:02:26');

-- 2023-03-07 12:14:03
