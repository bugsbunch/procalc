<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->id();
            $table->string('calc_id')->nullable();
            $table->string('uid')->nullable();
            $table->string('prop');
            $table->integer('order')->default(0);
            $table->string('size')->default('col-12');
            $table->string('title')->default('Заголовок');
            $table->string('description')->default('Описание');
            $table->boolean('title_visibility')->default(true);
            $table->boolean('description_visibility')->default(true);
            $table->integer('padding_top')->default(16);
            $table->integer('padding_bottom')->default(16);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elements');
    }
}
