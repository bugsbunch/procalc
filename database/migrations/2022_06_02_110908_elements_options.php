<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ElementsOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options_selects', function (Blueprint $table) {
            $table->id();
            $table->integer('element_id');
            $table->integer('selected')->default(1);
        });
        Schema::create('options_checkboxes', function (Blueprint $table) {
            $table->id();
            $table->integer('element_id');
            $table->json('selected_arr');
        });
        Schema::create('options_sliders', function (Blueprint $table) {
            $table->id();
            $table->integer('element_id');
            $table->integer('min')->default(1);
            $table->integer('step')->default(1);
            $table->integer('max')->default(100);
            $table->integer('value')->default(10);
            $table->integer('marks')->default(10);
            $table->string('tooltip')->default('focus');
        });
        Schema::create('options_inputs', function (Blueprint $table) {
            $table->id();
            $table->integer('element_id');
            $table->string('input_type')->nullable();
            $table->string('placeholder_text')->default('Подсказка');
            $table->boolean('placeholder_visibility')->default(false);
        });
        Schema::create('options_texts', function (Blueprint $table) {
            $table->id();
            $table->integer('element_id');
            $table->string('align')->nullable();
            $table->string('bold')->nullable();
            $table->string('italic')->nullable();
            $table->integer('size')->default(15);
            $table->text('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options_select');
        Schema::dropIfExists('options_checkbox');
        Schema::dropIfExists('options_slider');
        Schema::dropIfExists('options_input');
        Schema::dropIfExists('options_text');
    }
}
