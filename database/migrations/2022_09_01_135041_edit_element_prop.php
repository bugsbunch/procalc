<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditElementProp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('elements', function ($table) {
            $table->integer('prop')->change();
        });
        Schema::table('elements', function(Blueprint $table) {
            $table->renameColumn('prop', 'style_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('elements', function(Blueprint $table) {
            $table->renameColumn('style_id', 'prop');
        });
        Schema::table('elements', function ($table) {
            $table->string('prop')->change();
        });
    }
}
