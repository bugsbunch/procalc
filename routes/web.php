<?php

use App\Http\Controllers\AxiosController;
use App\Http\Controllers\CalcController;
use App\Http\Controllers\ExpressionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {

    Route::get('/axios/element/get', [AxiosController::class, 'elements_get'])->name('axios.element.get');
    Route::post('/axios/element/add', [AxiosController::class, 'elements_add'])->name('axios.element.add');
    Route::post('/axios/element/splice', [AxiosController::class, 'elements_splice'])->name('axios.element.splice');
    Route::post('/axios/element/order', [AxiosController::class, 'elements_order'])->name('axios.element.order');
    Route::post('/axios/element/update', [AxiosController::class, 'element_update'])->name('axios.element.update');
    Route::post('/axios/option/update', [AxiosController::class, 'option_update'])->name('axios.option.update');
    Route::post('/axios/option/delete', [AxiosController::class, 'option_delete'])->name('axios.option.delete');
    Route::post('/axios/option/add', [AxiosController::class, 'option_add'])->name('axios.option.add');
    Route::post('/axios/image/upload', [AxiosController::class, 'image_upload'])->name('axios.image.upload');

    Route::get('/calc', [CalcController::class, 'index'])->name('calc.index');
    Route::post('/calc/add', [CalcController::class, 'add'])->name('calc.add');
    Route::get('/calc/{uuid}', [CalcController::class, 'edit'])->name('calc.edit');
    Route::get('/calc/{uuid}/formula', [CalcController::class, 'edit'])->name('calc.edit.formula');


    Route::get('/expression', [ExpressionController::class, 'index'])->name('expression.index');

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->middleware(['auth'])->name('dashboard');
});
require __DIR__.'/auth.php';
