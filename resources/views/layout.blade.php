<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
</head>
<body id="body">
@yield('content')
{{--<link rel="stylesheet" href="/css/app.css">--}}
<link rel="stylesheet" href="/css/calc.css">
<script src="/js/app.js"></script>
</body>
</html>
