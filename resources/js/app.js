// require('./bootstrap');
//
// import Alpine from 'alpinejs';
//
// window.Alpine = Alpine;
//
// Alpine.start();

require('./bootstrap');

import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';

Vue.config.devtools = true;
Vue.config.performance = true;
Vue.use(Vuex);
Vue.use(VueRouter);


import Parent from './Parent.vue';
import Index from './Index.vue';
import Formula from './Formula.vue';

let spit_url = window.location.href.split("/");
const calc_uuid = spit_url[4];
const base_url = '/calc/' + calc_uuid;
const db_names = {
    'size': { db: 'elements', prop: 'size' },
    'label_title_text': { db: 'elements', prop: 'title' },
    'label_title_visibility': { db: 'elements', prop: 'title_visibility' },
    'label_description_text': { db: 'elements', prop: 'description' },
    'label_description_visibility': { db: 'elements', prop: 'description_visibility' },
    'padding_top': { db: 'elements', prop: 'padding_top' },
    'padding_bottom': { db: 'elements', prop: 'padding_bottom' },
    'selected': { db: 'options_selects', prop: 'selected' },
    'selected_arr': { db: 'options_checkboxes', prop: 'selected_arr' },
    'value': { db: 'options_sliders', prop: 'value' },
    'min': { db: 'options_sliders', prop: 'min' },
    'max': { db: 'options_sliders', prop: 'max' },
    'step': { db: 'options_sliders', prop: 'step' },
    'marks': { db: 'options_sliders', prop: 'marks' },
    'tooltip': { db: 'options_sliders', prop: 'tooltip' },
    'types': { db: 'options_inputs', prop: 'input_type' },
    'placeholder_visibility': { db: 'options_inputs', prop: 'placeholder_visibility' },
    'placeholder_text': { db: 'options_inputs', prop: 'placeholder_text' },
    'button_align': { db: 'options_buttons', prop: 'text_align' },
    'button_name': { db: 'options_buttons', prop: 'button_name' },
    'bold': { db: 'options_texts', prop: 'bold' },
    'italic': { db: 'options_texts', prop: 'italic' },
    'font_size': { db: 'options_texts', prop: 'size' },
    'text': { db: 'options_texts', prop: 'text' },
    'action': { db: 'options_buttons', prop: 'action' },
    'contacts_phone': { db: 'options_buttons', prop: 'phone' },
    'contacts_email': { db: 'options_buttons', prop: 'email' },
    'contacts_url': { db: 'options_buttons', prop: 'url' },
    'result_title': { db: 'options_results', prop: 'title' },
    'result_post': { db: 'options_results', prop: 'post' },
};

const routes = [
    { path: base_url + '/', component: Index, name: 'index', },
    { path: base_url + '/formula', component: Formula, name: 'formula' }
];

const router = new VueRouter({
    mode: 'history',
    routes: routes,
});

function add_to_db(element) {
    element.calc_uuid = calc_uuid;
    let json_element = JSON.stringify(element);
    window.axios.post('/axios/element/add', { element: json_element }).then(res => {
        return res.status === 200;
    });
}

function splice_in_db(uid) {
    window.axios.post('/axios/element/splice', { uid: uid }).then(res => {
        return res.status === 200;
    });
}

function update_orders(elements = []) {
    let output = [];
    elements.forEach((el, index) => {
        output.push({
            uid: el.uid,
            order: index
        })
    });
    window.axios.post('/axios/element/order', { element: JSON.stringify(output) }).then(res => {
        return res.status === 200;
    });
}

const store = new Vuex.Store({
    state: {
        elements: [],
        calc_uuid: calc_uuid,
        formula_blocks: [
            {
                index: 1,
                equals: []
            }
        ]
    },
    mutations: {
        add_element(state, elements) {
            let added_element = elements.filter(x => !state.elements.includes(x))[0];
            let alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
            added_element.word = alphabet[state.elements.length];
            if(typeof added_element !== "undefined"){
                add_to_db(added_element);
            }
            update_orders(elements);
            state.elements = elements;
        },
        splice_element(state, payload) {
            splice_in_db(payload.uid);
            state.elements.splice(payload.index, 1);
            if(state.elements.length > 0){
                update_orders(state.elements);
            }
        },
        clone_element(state, payload) {
            add_to_db(payload.clone);
            state.elements.insert(payload.index, payload.clone);
            update_orders(state.elements);
        },
        add_db_elements(state, elements) {
            elements.forEach(el => {
                state.elements.push(el);
            });
        },
        change_property(state, payload) {
            if(payload.property[0] === 'text') {
                payload.value = payload.value.replace('&nbsp;', ' ');
            }
            console.log(payload);
            if(payload.property[0] !== 'options' && payload.value !== '' && payload.value !== null) {
                window.axios.post('/axios/element/update', {
                    title: db_names[payload.property.join('_')],
                    uid: payload.uid,
                    value: payload.value
                }).then(res => {
                    return res.status === 200;
                });
            } else if(payload.property[0] === 'options' && payload.value !== '' && payload.value !== null) {
                if(payload.property[2] === 'text') {
                    window.axios.post('/axios/option/update', {
                        index: parseInt(payload.property[1]) + 1,
                        uid: payload.uid,
                        text: payload.value,
                    }).then(res => {
                        return res.status === 200;
                    });
                } else if(payload.property[2] === 'value') {
                    window.axios.post('/axios/option/update', {
                        index: parseInt(payload.property[1]) + 1,
                        uid: payload.uid,
                        value: payload.value,
                    }).then(res => {
                        return res.status === 200;
                    });
                } else if(payload.property[2] === 'disable_value') {
                    window.axios.post('/axios/option/update', {
                        index: parseInt(payload.property[1]) + 1,
                        uid: payload.uid,
                        disable_value: parseInt(payload.value),
                    }).then(res => {
                        return res.status === 200;
                    });
                }
            }
        },
        option_delete(state, payload) {
            window.axios.post('/axios/option/delete', {
                index: payload.index,
                uid: payload.uid,
            }).then(res => {
                return res.status === 200;
            });
        },
        option_add(state, payload) {
            window.axios.post('/axios/option/add', {
                index: payload.index,
                uid: payload.uid,
                text: payload.text
            }).then(res => {
                return res.status === 200;
            });
        },
        default_value(state, payload) {
            let element = state.elements.filter(x => x.uid === payload.uid)[0];
            element[payload.type] = payload.value;
        },
        add_to_equal(state, payload) {
            let block = state.formula_blocks.filter(x => x.index === payload.index)[0];
            if(block.equals.length > 0) {
                let last_eq = block.equals[block.equals.length - 1];
                if (last_eq.type !== payload.value.type) {
                    block.equals.push(payload.value);
                }
            } else {
                block.equals.push(payload.value);
            }
        }
    }
});

window.axios.get('/axios/element/get', { params: { uuid: calc_uuid  }}).then(res => {
    if(res.status === 200) {
        if (res.data.length > 0) {
            store.commit('add_db_elements', res.data);
            let start_window = document.querySelector('.start-window');
            if(start_window !== null) {
                start_window.classList.remove('show');
            }
        }
    }
});

Vue.directive('scroll', {
    inserted: function (el, binding) {
        let f = function (evt) {
            if (binding.value(evt, el)) {
                window.removeEventListener('scroll', f)
            }
        };
        window.addEventListener('scroll', f)
    }
});

const app = new Vue({
    router,
    store: store,
    el: '#app',
    render: h => h(Parent)
});
