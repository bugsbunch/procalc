<?php

namespace App\Http\Controllers;

use App\Models\Calc;
use App\Models\Element;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CalcController extends Controller
{
    protected function index()
    {
        return view('calc_list');
    }

    protected function edit($uuid)
    {
        $calc = Calc::with('elements')->where('uuid', $uuid)->first();

        return view('index');
    }

    protected function add()
    {
        return view('index');
    }
}
