<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class ExpressionController extends Controller
{

    protected $test_js_output = [
        'data' => [
            'param_1' => 2,
            'param_2' => 3,
            'param_3' => 4,
        ],
        'expression' => '(data.param_3 + data.param_2) * data.param_1'
    ];

    protected function index()
    {
        $json = json_encode($this->test_js_output);
        $decode = json_decode($json);

        $expressionLanguage = new ExpressionLanguage();
        $result = $expressionLanguage->evaluate(
            $decode->expression,
            [
                'data' => $decode->data,
            ]
        );

        var_dump($result);
    }
}
