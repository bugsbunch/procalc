<?php

namespace App\Http\Controllers;

use App\Models\Element;
use App\Models\Option;
use App\Models\options_button;
use App\Models\options_checkbox;
use App\Models\options_image;
use App\Models\options_input;
use App\Models\options_select;
use App\Models\options_slider;
use App\Models\options_text;
use App\Models\Style;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Imagick;

class AxiosController extends Controller
{

    protected function elements_get(Request $request)
    {
        $uuid = $request->input('uuid');
        if ($uuid !== null) {
            if (!is_string($uuid) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}$/', $uuid) !== 1)) {
                return response('invalid uuid', 400);
            }
        }

        $output = [];

        $with = ['style', 'options', 'option_selects', 'option_checkboxes', 'option_sliders', 'option_inputs', 'option_texts', 'option_images', 'option_buttons'];

        if ($uuid !== null) {
            $elements = Element::with($with)->where('calc_id', $uuid)->orderBy('order')->get();
        } else {
            $elements = Element::with($with)->where('calc_id', null)->get();
        }

        if(count($elements) > 0) {
            foreach ($elements as $element)
            {
                $temp = [];
                $temp['name'] = $element->style->name;
                $temp['icon'] = $element->style->icon;
                $temp['color'] = $element->style->color;
                $temp['prop'] = $element->style->prop;
                $temp['uid'] = $element->uid;
                $temp['size'] = $element->size;
                $temp['word'] = $element->word;
                $temp['padding'] = ['top' => $element->padding_top, 'bottom' => $element->padding_bottom];
                $temp['label'] = [
                    'title' => ['visibility' => (bool)$element->title_visibility, 'text' => $element->title],
                    'description' => ['visibility' => (bool)$element->description_visibility, 'text' => $element->description]
                ];
                switch ($element->style->prop)
                {
                    case 'select':
                    case 'radio':
                        $temp['selected'] = $element->option_selects[0]->selected;
                        $temp_options = [];
                        foreach ($element->options as $i => $option)
                        {
                            $temp_options[$i]['id'] = $option->id;
                            $temp_options[$i]['index'] = $option->index;
                            $temp_options[$i]['text'] = $option->text;
                            $temp_options[$i]['value'] = $option->value;
                            $temp_options[$i]['disable_value'] = $option->disable_value;
                        }
                        $temp['options'] = $temp_options;
                        break;
                    case 'slider':
                        $temp['min'] = $element->option_sliders[0]->min;
                        $temp['max'] = $element->option_sliders[0]->max;
                        $temp['value'] = $element->option_sliders[0]->value;
                        $temp['step'] = $element->option_sliders[0]->step;
                        $temp['marks'] = $element->option_sliders[0]->marks;
                        $temp['tooltip'] = $element->option_sliders[0]->tooltip;
                        break;
                    case 'checkbox':
                        $temp['selected'] = json_decode($element->option_checkboxes[0]->selected_arr);
                        $temp_options = [];
                        foreach ($element->options as $i => $option)
                        {
                            $temp_options[$i]['id'] = $option->id;
                            $temp_options[$i]['index'] = $option->index;
                            $temp_options[$i]['text'] = $option->text;
                            $temp_options[$i]['value'] = $option->value;
                            $temp_options[$i]['disable_value'] = $option->disable_value;
                        }
                        $temp['options'] = $temp_options;
                        break;
                    case 'input':
                        $temp['types'] = $element->option_inputs[0]->input_type;
                        $temp['placeholder'] = ['visibility' => (bool)$element->option_inputs[0]->placeholder_visibility, 'text' => $element->option_inputs[0]->placeholder_text];
                        break;
                    case 'text':
                        $temp['text'] = $element->option_texts[0]->text;
                        $temp['text_align'] = $element->option_texts[0]->align;
                        $temp['bold'] = $element->option_texts[0]->bold;
                        $temp['italic'] = $element->option_texts[0]->italic;
                        $temp['font_size'] = $element->option_texts[0]->size;
                        break;
                    case 'image':
                        $temp['path'] = $element->option_images[0]->path;
                        $temp['file_name'] = $element->option_images[0]->name;
                        break;
                    case 'button':
                        $temp['action'] = $element->option_buttons[0]->action;
                        $temp['button_align'] = $element->option_buttons[0]->text_align;
                        $temp['button_name'] = $element->option_buttons[0]->button_name;
                        $temp['contacts'] = ['email' => $element->option_buttons[0]->email, 'phone' => $element->option_buttons[0]->phone, 'url' => $element->option_buttons[0]->url];
                        break;
//                    case 'result':
//                        $temp['name'] = 'Результат';
//                        $temp['icon'] = 'icon-formula';
//                        $temp['color'] = '#f9d94a';
//                        break;
                }

                $output[] = $temp;
            }
        }

        return response()->json($output, 200, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE);
    }

    protected function elements_add(Request $request)
    {
        $input_json = $request->input('element');
        if(!$this->isValidJSON($input_json)) {
            return response('invalid format', 400);
        }

        $input = json_decode($input_json);

        $style = Style::where('prop', $input->prop)->first();

        $element = Element::create([
            'calc_id' => $input->calc_uuid,
            'uid' => $input->uid,
            'style_id' => $style->id,
            'size' => $input->size,
            'padding_top' => $input->padding->top,
            'padding_bottom' => $input->padding->bottom,
            'title' => $input->label->title->text,
            'title_visibility' => $input->label->title->visibility,
            'description' => $input->label->description->text,
            'description_visibility' => $input->label->description->visibility,
            'word' => $input->word,
        ]);

        switch ($style->prop)
        {
            case 'radio':
            case 'select':
                options_select::create(['element_id' => $element->id, 'selected' => $input->selected]);
                foreach ($input->options as $o)
                {
                    $option = new Option;
                    $option->element_id = $element->id;
                    $option->index = $o->index;
                    $option->text = $o->text;
                    $option->value = $o->value;
                    $option->disable_value = $o->disable_value;
                    $option->save();
                }
                break;
            case 'slider':
                options_slider::create([
                    'element_id' => $element->id,
                    'min' => $input->min,
                    'max' => $input->max,
                    'value' => $input->value,
                    'step' => $input->step,
                    'marks' => $input->marks,
                    'tooltip' => $input->tooltip,
                ]);
                break;
            case 'checkbox':
                options_checkbox::create(['element_id' => $element->id, 'selected_arr' => json_encode($input->selected)]);
                foreach ($input->options as $o)
                {
                    $option = new Option;
                    $option->element_id = $element->id;
                    $option->index = $o->index;
                    $option->text = $o->text;
                    $option->save();
                }
                break;
            case 'input':
                options_input::create([
                    'element_id' => $element->id,
                    'input_type' => $input->types,
                    'placeholder_text' => $input->placeholder->text,
                    'placeholder_visibility' => $input->placeholder->visibility,
                ]);
                break;
            case 'text':
                options_text::create([
                    'element_id' => $element->id,
                    'text' => $input->text,
                    'align' => $input->text_align,
                    'bold' => $input->bold,
                    'italic' => $input->italic,
                    'size' => $input->font_size,
                ]);
                break;
            case 'image':
                options_image::create([
                    'element_id' => $element->id,
                ]);
                break;
            case 'button':
                options_button::create([
                    'element_id' => $element->id,
                ]);
                break;
//            case 'html':
//                $temp['name'] = 'HTML-код';
//                $temp['icon'] = 'icon-html';
//                $temp['color'] = '#f9d94a';
//                break;
        }

        $element->save();

        return response(1);
    }

    protected function elements_order(Request $request)
    {
        $input_json = $request->input('element');
        if(!$this->isValidJSON($input_json)) {
            return response('invalid format', 400);
        }

        $input = json_decode($input_json);
        if(count($input) > 0){
            foreach ($input as $item)
            {
                $element = Element::where('uid', $item->uid)->first();
                if (is_object($element)){
                    $element->order = $item->order;
                    $element->save();
                }
            }
        }

        return response(1);
    }

    protected function element_update(Request $request)
    {
        $title = $request->input('title');
        $uid = $request->input('uid');
        $value = $request->input('value');

        if(is_array($title)) {
            $element = Element::where('uid', $uid)->first();
            if (is_object($element)){
                $db = $title['db'];
                $prop = $title['prop'];
                if($db === 'elements') {
                    $element->$prop = $value;
                    $element->save();
                } else {
                    DB::table($db)->where('element_id', $element->id)->update([$prop => $value]);
                }
            }
        }

        return response(1);
    }

    protected function option_update(Request $request)
    {
        $index = $request->input('index');
        $uid = $request->input('uid');
        $text = $request->input('text');
        $value = $request->input('value');
        $disable_value = $request->input('disable_value');

        $element = Element::where('uid', $uid)->first();
        if (is_object($element)) {
            $option = Option::where('element_id', $element->id)->where('index', (int)$index)->first();
            if (is_object($option) && $text && !empty($text)){
                $option->text = $text;
            }
            if (is_object($option) && $value && !empty($value)){
                $option->value = $value;
            }
            if (is_object($option) && $disable_value && !empty($disable_value)){
                $option->disable_value = $disable_value;
            }
            $option->save();
        }

        return response(1);
    }

    protected function option_delete(Request $request)
    {
        $index = $request->input('index');
        $uid = $request->input('uid');

        $element = Element::where('uid', $uid)->first();
        if (is_object($element)) {
            $option = Option::where('element_id', $element->id)->where('index', (int)$index)->first();
            if (is_object($option)){
                $option->delete();
            }
        }

        return response(1);
    }

    protected function option_add(Request $request)
    {
        $index = $request->input('index');
        $uid = $request->input('uid');
        $text = $request->input('text');

        $element = Element::where('uid', $uid)->first();
        if (is_object($element)) {
            Option::create([
                'index' => $index,
                'element_id' => $element->id,
                'text' => $text
            ]);
        }

        return response(1);
    }

    protected function elements_splice(Request $request)
    {
        $uid = $request->input('uid');
        if(!is_string($uid) or strlen($uid) !== 10) {
            return response('invalid uid', 400);
        }

        $element = Element::with('style')->where('uid', $uid)->first();
        if(is_object($element)) {
            if(in_array($element->style->prop, ['select', 'radio', 'checkbox'])) {
                foreach ($element->options as $option) {
                    $option->delete();
                }
            }
            if(in_array($element->style->prop, ['select', 'radio'])) {
                foreach ($element->option_selects as $option_select) {
                    $option_select->delete();
                }
            }
            if(in_array($element->style->prop, ['checkbox'])) {
                foreach ($element->option_checkboxes as $option_checkboxes) {
                    $option_checkboxes->delete();
                }
            }
            if(in_array($element->style->prop, ['slider'])) {
                foreach ($element->option_sliders as $option_sliders) {
                    $option_sliders->delete();
                }
            }
            if(in_array($element->style->prop, ['input'])) {
                foreach ($element->option_inputs as $option_inputs) {
                    $option_inputs->delete();
                }
            }
            if(in_array($element->style->prop, ['text'])) {
                foreach ($element->option_texts as $option_texts) {
                    $option_texts->delete();
                }
            }
            if(in_array($element->style->prop, ['image'])) {
                foreach ($element->option_images as $option_images) {
                    $path = $_SERVER['DOCUMENT_ROOT'] . str_replace('/storage/image_model/', '/storage/app/public/image_model/', $option_images->path);
                    if (file_exists($path)) {
                        unlink($path);
                    }
                    $option_images->delete();
                }
            }
            if(in_array($element->style->prop, ['button'])) {
                foreach ($element->option_buttons as $option_buttons) {
                    $option_buttons->delete();
                }
            }
            $element->delete();
        }
        return response(1);
    }

    function isValidJSON($string) {
        $arr = json_decode($string);
        try {
            if(is_array($arr)){
                return $arr[0]->uid;
            } else {
                return $arr->uid;
            }
        } catch (\Exception $e){
            return false;
        }
    }

    protected function image_upload(Request $request)
    {
        $uuid = $request->input('uuid');
        $element_id = $request->input('element_id');
        $image = $request->file('file');
        $file_name = $image->getClientOriginalName();

        if ($uuid !== null) {
            if (!is_string($uuid) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}$/', $uuid) !== 1)) {
                return response('invalid uuid', 400);
            }
        }

        $element = Element::where('uid', $element_id)->first();
        $image_option = options_image::where('element_id', $element->id)->first();

        $imagick = new Imagick($image->getRealPath());

        $imagick->stripImage();
        $imagick->setImageCompressionQuality(95);
        $imagick->setSamplingFactors(array('2x2', '1x1', '1x1'));
        $imagick->setInterlaceScheme(Imagick::INTERLACE_PLANE);
        if ($imagick->getImageColorspace() !== Imagick::COLORSPACE_RGB) {
            $imagick->transformimagecolorspace(Imagick::COLORSPACE_RGB);
        }
        if($imagick->getImageWidth() > $imagick->getImageHeight() && $imagick->getImageWidth() > 1500){
            $imagick->scaleImage(1500, 0);
        } elseif ($imagick->getImageWidth() < $imagick->getImageHeight() && $imagick->getImageHeight() > 1500){
            $imagick->scaleImage(0, 1500);
        } elseif ($imagick->getImageWidth() === $imagick->getImageHeight() && $imagick->getImageWidth() > 1500){
            $imagick->scaleImage(1500, 1500);
        }
        $imagick->writeImage('jpg:' . storage_path('/app/public/image_model/' . $uuid . '/' . $element_id . '.jpg'));
        $imagick->clear();
        $imagick->destroy();

        $image_option->path = '/storage/image_model/' . $uuid . '/' . $element_id . '.jpg';
        $image_option->name = $file_name;
        $image_option->save();

        return response('Done', 200);
    }
}
