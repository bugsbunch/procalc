<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class options_checkbox extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $attributes = [
        'selected_arr' => '[1]'
    ];
    protected $guarded = [];
    public function element()
    {
        return $this->belongsTo(Element::class);
    }
}
