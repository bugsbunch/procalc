<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class options_image extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;
    public function element()
    {
        return $this->belongsTo(Element::class);
    }
}
