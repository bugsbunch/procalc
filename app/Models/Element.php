<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;

    public function options()
    {
        return $this->hasMany(Option::class);
    }
    public function option_selects()
    {
        return $this->hasMany(options_select::class);
    }
    public function option_checkboxes()
    {
        return $this->hasMany(options_checkbox::class);
    }
    public function option_sliders()
    {
        return $this->hasMany(options_slider::class);
    }
    public function option_inputs()
    {
        return $this->hasMany(options_input::class);
    }
    public function option_texts()
    {
        return $this->hasMany(options_text::class);
    }
    public function option_images()
    {
        return $this->hasMany(options_image::class);
    }
    public function option_buttons()
    {
        return $this->hasMany(options_button::class);
    }
    public function option_results()
    {
        return $this->hasMany(options_result::class);
    }
    public function calc()
    {
        return $this->belongsTo(Calc::class, 'uuid', 'calc_id');
    }
    public function style()
    {
        return $this->belongsTo(Style::class);
    }
}
