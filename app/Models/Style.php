<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;

    public function elements()
    {
        return $this->hasMany(Element::class);
    }
}
