<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class options_text extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $attributes = [
        'text' => ''
    ];
    protected $guarded = [];
    public function element()
    {
        return $this->belongsTo(Element::class);
    }
}
